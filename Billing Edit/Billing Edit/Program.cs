﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Billing_Edit
{
    class Program
    {
        /*
         *                                                         -[Billing Edit 2.0]-    
         *                                                                  
         *      This version of the Billing Edit is structured to allow for Databases to added in by adding Rows or Columns into SQL Tables
         *      rather than having to add different variations of Hard Coded values i.e. PublicVars. 
         *      
         *      Instead, by adding a single row of information, we can check any number of Brightree Databases and allow or ignore 
         *      certain methods to check various parts of a Sales Order before putting it into Confirmation.  Rather than having a huge 
         *      list of Public Variables for each database, the values are saved on a SQL Table that can easily be added to or remove 
         *      databases we do not want to check any longer.  
         *      
         *      In addition, a second table contains true/false values for database specific methods that can be changed whenever we need to, 
         *      without stopping the Billing Edit, nor requiring us to push updates through BitBucket for every single update. 
         *      
         *      These values are stored in an object that I called "SalesOrderSearchObjects" which contains all of this information for every single order we check.       
         */
        
        static void Main(string[] args)
        {
            try
            {
                 //Get the publicVars for every Database in the SQL Table, but keep it out of the loop so we do not make this same request 100s of times
                PublicVars.createPublicVarsObjects();
            
                //For each database we want to look at, get orders for both of them in this initial setup.
                foreach (publicVarsObject publicVarsObj in PublicVars.publicVarsObjects)
                {
                    //create client objects for this database
                    ServiceInitializers.serviceClients clients = ServiceInitializers.createServices(publicVarsObj.username, publicVarsObj.password);

                    //Get a list of permissions for this database.
                    List<string> permissions = Permissions.createPermissions(publicVarsObj.database);

                    //Get Orders -- currently only grabs 4.0 and .39, based on Tyler.dbo.BillingEdit_WIPStateKeys, add rows in that SQL table to add to remove WIPStates
                    WIPSearcher.executeWIPSearchMethod(publicVarsObj, permissions, clients);
                }                    
                
                if(WIPSearcher.selectedOrders.Count > 0)
                {
                    //Loop through the orders and do all of the checks nessecary to confirm the order
                    for (int i = 0; i < WIPSearcher.selectedOrders.Count; i++)
                    {
                        //set Kickback List back to count 0 and make isEligible = true, and make sure we have the proper Client(s) for the Database we are interacting with.
                        PublicVars.resetVariables(i);

                        //Fetch the values for this specific Sales Order and Patient
                        salesOrderPatientFetches.fetchInfo(WIPSearcher.selectedOrders[i], WIPSearcher.selectedPatients[i]);

                        //If the order is not in the proper WIP State, move onto the next one.
                        if (VerifyOrderConditions.checkCurrentOrderWIPState((int)salesOrderPatientFetches.SOFetch.SalesOrderWIPInfo.WIPStateKey) == false)
                            continue;

                        //Check various validations on the Sales Order
                        salesOrderChecks.salesOrderCheck(salesOrderPatientFetches.SOFetch, salesOrderPatientFetches.PTPrimaryPayor);
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Checks relating to CMNs on Sales Orders
                        soCMNChecks.executeCMNChecks((int)salesOrderPatientFetches.SOFetch.BrightreeID);
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Checks relating to PARs on Sales Orders
                        soPARChecks.executePARChecks((int)salesOrderPatientFetches.SOFetch.BrightreeID);
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Checks relating to Items in the Sales Order
                        lineItemValidations.executeLineItemChecks(salesOrderPatientFetches.SOFetch);
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Checks specifically for Medicare orders in StLukes -- uses the Payor crosswalk to get the Electronic ID of Medciare, 431.
                        if (SQLMethods.checkPayorCrosswalk(salesOrderPatientFetches.PTPrimaryPayor.PayorKey) == "431")
                        {
                            MedicareChecks.checkMedicareConditions(salesOrderPatientFetches.SOFetch);
                            if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };
                        }

                        //Eligible API calls, currently only Medicare
                        eligibleAPI.evaluatePolicy(salesOrderPatientFetches.PTPrimaryPayor, salesOrderPatientFetches.PTFetch);
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Check for Blue Payors
                        BluesCheck.bluesCheckMethod();
                        if (evaluateOrder.checkKickbackStatus(PublicVars.isEligible) == true) { evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible); continue; };

                        //Evaluate the Order to try and confirm the order or update it.
                        evaluateOrder.evaluateSalesOrder(salesOrderPatientFetches.SOFetch, PublicVars.kickbackReasons, PublicVars.isEligible);
                    }    
                }               
            } 
            catch(Exception e)
            {
                emailNotification.SendErrorEmail("rhumphries@qmes.com", e.Message);
                Twilio.sendMessage("Billing Edit has stopped - "+e.Message+". Please Restart");
            }
        }                   
    }
}

