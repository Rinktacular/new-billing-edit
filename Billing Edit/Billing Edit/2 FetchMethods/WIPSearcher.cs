﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SQL = System.Data;

namespace Billing_Edit
{
    public class WIPSearcher
    {
        public static List<salesOrderSearchObject> selectedOrders = new List<salesOrderSearchObject>();
        public static List<int> selectedPatients = new List<int>();
          
        public static void executeWIPSearchMethod(publicVarsObject publicVars, List<string> permissions, ServiceInitializers.serviceClients clients)
        {            
            
            int i = 0;
            //Set the globals back to an empty list if this is a second iteraiton.
            selectedOrders.Clear();
            selectedOrders.Clear();
            //Currently should only be fetching .39 and 4.0 WIPStates
            foreach(int WIPState in publicVars.WIPSToSearch)
            {
                Console.WriteLine("Obtaining {0} ({1}) orders", publicVars.database, WIPState);
                //Create a search based on WIP State
                soService.SalesOrderSearchRequest soSearch = new soService.SalesOrderSearchRequest();
                soSearch.WIPUserTaskReason = new soService.LookupValue();
                soSearch.WIPUserTaskReason.ID = WIPState;

                //Sort the results by Brightree ID
                List<soService.SalesOrderSortParameter> sortParam = new List<soService.SalesOrderSortParameter>();
                soService.SalesOrderSortParameter soSort = new soService.SalesOrderSortParameter();
                soSort.SortField = soService.SalesOrderSortFields.BrightreeID;
                soSort.SortOrder = soService.SortOrder.Ascending;

                sortParam.Add(soSort);

                //Send the search request to Brightree
                var soSearcher = clients.soAction.SalesOrderSearch(soSearch, sortParam.ToArray(), 1000, 1);

                //For each response (order) from soSearcher...
                foreach(soService.SalesOrderSearchResponse response in soSearcher.Items)
                {
                    salesOrderSearchObject searchObject = new salesOrderSearchObject(response.BrightreeID, publicVars, permissions, clients);
                    //Add the Sales Order Number
                    selectedOrders.Add(searchObject);
                    //Add the PatientID from the Sales Order
                    selectedPatients.Add(response.Patient.ID);
                    i++;
                }
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Complete. {0} Total {1} Orders", i, publicVars.database);
            Console.ForegroundColor = ConsoleColor.White;
        }
        
    }
}
