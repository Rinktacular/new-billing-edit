﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SQL = System.Data;

namespace Billing_Edit
{
    public class PublicVars
    {
        public static bool isEligible = true;
        public static List<string> kickbackReasons = new List<string>();
        public static decimal allowedAmnt;

        public static salesOrderSearchObject currentSalesOrderObject;
        public static publicVarsObject currentPublicVarsObject = new publicVarsObject();
        public static ServiceInitializers.serviceClients currentClientObject = new ServiceInitializers.serviceClients();

       
        public static List<publicVarsObject> publicVarsObjects = new List<publicVarsObject>();
        public ServiceInitializers.serviceClients clients;


        public static void createPublicVarsObjects()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Initialized");
            PublicVars.publicVarsObjects = getPublicVars();            
        }

        public static void resetVariables(int i )
        {
            PublicVars.isEligible = true;
            PublicVars.kickbackReasons.Clear();
            PublicVars.currentSalesOrderObject = WIPSearcher.selectedOrders[i];
            PublicVars.currentPublicVarsObject = WIPSearcher.selectedOrders[i]._publicVars;
            PublicVars.currentClientObject = WIPSearcher.selectedOrders[i]._clients;
            Console.WriteLine("{0} Orders Remaining", WIPSearcher.selectedOrders.Count - i);
        }       

        public static List<int> getWIPS(publicVarsObject vars)
        {
            List<int> WIPSToSearch = new List<int>();
            WIPSToSearch.Add(vars.preConWIP);
            WIPSToSearch.Add(vars.reviewWIP);
            return WIPSToSearch;
            
        }
        public static List<publicVarsObject> getPublicVars()
        {
            List<publicVarsObject> publicVarsObjects = new List<publicVarsObject>();
            SqlConnection cnn = new SqlConnection("Data Source=104.209.184.65;Connection Timeout=60;Initial Catalog=BrightreeData;Persist Security Info=True;User ID=rhumphries;Password=Qmes2015");
            string sqlCommand = "SELECT * FROM [Tyler].[dbo].[BillingEdit_PublicVars]";

            cnn.Open();
            SQL.DataSet ds = new SQL.DataSet();
            SQL.DataTable dtable = new SQL.DataTable();
            SqlDataAdapter dscmd = new SqlDataAdapter(sqlCommand, cnn);

            dscmd.Fill(dtable);

            for (int i = 0; i < dtable.Rows.Count; i++)
            {
                publicVarsObject publicVars = new publicVarsObject();
                publicVars.database = dtable.Rows[i]["DatabaseName"].ToString();
                publicVars.username = dtable.Rows[i]["BrightreeUsername"].ToString();
                publicVars.password = dtable.Rows[i]["BrightreePassword"].ToString();
                publicVars.preConWIP = Convert.ToInt32(dtable.Rows[i]["PreConWIP"].ToString());
                publicVars.reviewWIP = Convert.ToInt32(dtable.Rows[i]["AutoReviewWIP"].ToString());
                publicVars.kickbackWIP = Convert.ToInt32(dtable.Rows[i]["KickbackWIP"].ToString());
                publicVars.confirmWIP = Convert.ToInt32(dtable.Rows[i]["ConfirmWIP"].ToString());
                publicVars.WIPSToSearch = getWIPS(publicVars);
                publicVarsObjects.Add(publicVars);
            }           

            return publicVarsObjects;
        }
    }

    
}
