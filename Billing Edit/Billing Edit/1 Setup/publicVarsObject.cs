﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    public class publicVarsObject
    {
        public List<int> WIPSToSearch;
        //Live Database Credentials
        public string username;
        public string password;
        public string database;

        //WIPStateKeys
        //.39 Pre Con Needs CMN
        public int preConWIP;
        //4.0 auto confirm review
        public int reviewWIP;
        //4.1 auto confirm kickback 
        public int kickbackWIP;
        //4.2 auto confirm pass
        public int confirmWIP;           
    }
}
