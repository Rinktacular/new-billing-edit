﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Billing_Edit
{
    public class ServiceInitializers
    {     
        public struct serviceClients
        {
            public soService.SalesOrderServiceClient soAction;
            public ptService.PatientServiceClient ptAction;
            public docService.DocumentationServiceClient docAction;
            public insService.InsuranceServiceClient insAction;
            public cfService.CustomFieldServiceClient cfAction;
        }

        public static serviceClients createServices(string username, string password)
        {
            serviceClients serviceClient = new serviceClients();
            serviceClient.soAction = ServiceInitializers.createSalesOrderServiceClient(username, password);
            serviceClient.ptAction = ServiceInitializers.createPatientServiceClient(username, password);
            serviceClient.docAction = ServiceInitializers.createDocumentationServiceClient(username, password);
            serviceClient.insAction = ServiceInitializers.createInsuranceServiceClient(username, password);
            serviceClient.cfAction = ServiceInitializers.createCustomFieldServiceClient(username, password);
            return serviceClient;
        }
        
        private static cfService.CustomFieldServiceClient createCustomFieldServiceClient(string username, string password)
        {
            BasicHttpBinding mybinding = new BasicHttpBinding();
            mybinding.MaxReceivedMessageSize = 65536 * 2;
            mybinding.Security.Mode = BasicHttpSecurityMode.Transport;
            mybinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100/CustomFieldService/CustomFieldService.svc");
            cfService.CustomFieldServiceClient client = new cfService.CustomFieldServiceClient();
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;
            return client;
        }

        private static docService.DocumentationServiceClient createDocumentationServiceClient(string username, string password)
        {
            BasicHttpBinding mybinding = new BasicHttpBinding();
            mybinding.MaxReceivedMessageSize = 65536 * 2;
            mybinding.Security.Mode = BasicHttpSecurityMode.Transport;
            mybinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/V0100-1508/DocumentationService/DocumentationService.svc");
            docService.DocumentationServiceClient client = new docService.DocumentationServiceClient();
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;
            return client;
        }

        private static insService.InsuranceServiceClient createInsuranceServiceClient(string username, string password)
        {
            BasicHttpBinding mybinding = new BasicHttpBinding();
            mybinding.MaxReceivedMessageSize = 65536 * 2;
            mybinding.Security.Mode = BasicHttpSecurityMode.Transport;
            mybinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1210/OrderEntryService/InsuranceService.svc");
            insService.InsuranceServiceClient client = new insService.InsuranceServiceClient();
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;
            return client;
        }

        private static ptService.PatientServiceClient createPatientServiceClient(string username, string password)
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1505/OrderEntryService/patientservice.svc");
            ptService.PatientServiceClient client = new ptService.PatientServiceClient();
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;
            return client;
        }

        private static soService.SalesOrderServiceClient createSalesOrderServiceClient(string username, string password)
        {
            BasicHttpBinding mybinding = new BasicHttpBinding();
            mybinding.MaxReceivedMessageSize = 65536 * 2;
            mybinding.Security.Mode = BasicHttpSecurityMode.Transport;
            mybinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1508/OrderEntryService/SalesOrderService.svc");
            soService.SalesOrderServiceClient client = new soService.SalesOrderServiceClient();
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;
            return client;
        }
    }
}
