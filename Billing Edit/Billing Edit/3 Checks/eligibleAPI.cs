﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    class eligibleAPI
    {
        public static void evaluatePolicy(ptService.PatientPayor patientPayor, ptService.Patient patient)
        {
            /*Take payor object as argument, match in crosswalk to know what method to call in the Eligiblity Project..
                
             *   if Medicare do medicare search
             *   take payor_id from table and use it as argument in eligiblity search, or have specific methods and anything that isnt one of those payors, send it to general eligiblity
            */
            Tuple<bool, string> eligibleResponse = Tuple.Create(true, string.Empty);
            string crosswalkResponse = SQLMethods.checkPayorCrosswalk(patientPayor.PayorKey);
            if(crosswalkResponse != "")
            {
                switch(crosswalkResponse)
                {
                    case "431":
                        //PA Medicare
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.medicareEligibility(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString());
                        break;
                    case "NYMCD":
                        //NY Medicaid
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.nyMedicaidEligibility(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString());
                        break;
                    case "MDMCD":
                        //MD Medicaid
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.marylandMedicaidEligibility(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString());
                        break;
                    case "NJMCD":
                        //NJ Medicaid
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.njMedicaidEligibility(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString());
                        break;
                    case "DEMCD":
                        //DE Medicaid
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.delawareMedicaidEligiblity(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString());
                        break;
                    default:
                        //General Eligiblity 
                        eligibleResponse = eligibilityMethodLibrary.Eligibility.GeneralEligibility(patient.PatientGeneralInfo.Name.First, patient.PatientGeneralInfo.Name.Last, patientPayor.Policy.PolicyNumber, patient.PatientGeneralInfo.BirthDate.ToString(), DateTime.Today.ToString(), crosswalkResponse);
                        break;
                }
                if (eligibleResponse.Item1 == false)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add(eligibleResponse.Item2);
                }
            }           
        }
    }
}
