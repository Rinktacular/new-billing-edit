﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class soPARChecks
    {
        public static docService.PAR parFetchInfo { get; set; }
        private static int countpars { get; set; }

        public static void executePARChecks(int salesOrderNumber)
        {
            executePARFetchMethod(salesOrderNumber);
        }

        private static void executePARFetchMethod(int salesOrderNumber)
        {
            docService.DataFetchServiceResponseUsingPAR parFetch = PublicVars.currentClientObject.docAction.PARFetchBySalesOrderBrightreeID(salesOrderNumber, docService.vAPIPARPARLoadChildrenBitFlags.LoadAll);
            bool activePAR = false;
            int countpars = 0;
            foreach(docService.PAR parResponse in parFetch.Items)
            {
                countpars++;
                if (parResponse.PARGeneralInfo.PARInitialDate <= salesOrderPatientFetches.SOFetch.DeliveryInfo.ActualDeliveryDateTime)
                    activePAR = true;
                if(parResponse.PARGeneralInfo.PARNumber == null)
                {
                    PublicVars.kickbackReasons.Add("PAR logged with no PAR Number");
                    PublicVars.isEligible = false;
                    return;
                }
                if(parResponse.PARGeneralInfo.PARNumber != null && parResponse.PARGeneralInfo.PARNumber.ToLower().Contains("no"))
                {
                    PublicVars.kickbackReasons.Add("PAR logged without Auth Number or reference description");
                    PublicVars.isEligible = false;
                    return;
                }
            }

            if(activePAR == false && countpars > 0)
            {
                PublicVars.kickbackReasons.Add("PAR initial date is before Actual Date");
                PublicVars.isEligible = false;
                return;
            }
        }
    }
}
