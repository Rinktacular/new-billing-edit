﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class VerifyOrderConditions
    {   
        public static bool checkSelectedOrdersCount(List<salesOrderSearchObject> selectedOrders)
        {
            if (selectedOrders.Count == 0)
            {
                Console.WriteLine("Order List is currently empty");
                return false;
            }
            else 
                return true;
        }

        public static bool checkPermissions(List<string> permissions)
        {
            if(permissions.Count == 0)
            {
                Console.WriteLine("Permissions List is empty");
                return false;
            }
            return true;
        }

        public static bool checkCurrentOrderWIPState(int WIPState)
        {
            if (WIPState != PublicVars.currentPublicVarsObject.reviewWIP && WIPState != PublicVars.currentPublicVarsObject.preConWIP)
            {
                Console.WriteLine("Order is not in the proper WIP State");
                return false;
            }
            return true;
        }
    }
}
