﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    class Permissions
    {       
        public static List<string> createPermissions(string database)
        {
            List<string> permissions = new List<string>();

            switch(database)
            {
                case "stlukeshs":
                    permissions = SQLMethods.getPermissions(database);
                    return permissions;
                case "OHH":
                    permissions = SQLMethods.getPermissions(database);
                    return permissions;
                default:
                    Console.WriteLine("Billing Edit does not support the {0} database", database);
                    return permissions;
            }
        }
    }
}
