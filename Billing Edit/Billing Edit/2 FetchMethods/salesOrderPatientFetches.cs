﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class salesOrderPatientFetches
    {
        public static soService.SalesOrder SOFetch = new soService.SalesOrder();
        public static ptService.Patient PTFetch = new ptService.Patient();
        public static soService.SalesOrderPayorSearchResponse SOPrimaryFetch = new soService.SalesOrderPayorSearchResponse();
        public static ptService.PatientPayor PTPrimaryPayor = new ptService.PatientPayor();

        //Maybe should get permissions here?
        public static void fetchInfo(salesOrderSearchObject salesOrderNumber, int patientKey)
        {
            salesOrderPatientFetches.soSearchMethod(salesOrderNumber._salesOrder);
            salesOrderPatientFetches.soPayorSearchMethod(salesOrderNumber._salesOrder);
            salesOrderPatientFetches.ptSearchMethod(patientKey);
            salesOrderPatientFetches.ptPrimaryPayorSearchMethod(patientKey);
        }

        private static void soPayorSearchMethod(int salesOrderNumber)
        {
            soService.SalesOrderPayorSearchRequest soPayorSearch = new soService.SalesOrderPayorSearchRequest();
            soPayorSearch.SOKey = salesOrderNumber;
            soPayorSearch.PayorLevelKey = 1;

            List<soService.SalesOrderPayorSortParameter> payorSortParam = new List<soService.SalesOrderPayorSortParameter>();
            var payorSorter = new soService.SalesOrderPayorSortParameter();
            payorSorter.SortField = soService.SalesOrderPayorSortField.PayorLvlKey;
            payorSorter.SortOrder = soService.SortOrder.Ascending;

            var payorSearch = PublicVars.currentClientObject.soAction.SalesOrderPayorSearch(soPayorSearch, payorSortParam.ToArray(), 1, 1);
            
            foreach(soService.SalesOrderPayorSearchResponse payor in payorSearch.Items)
            {
                SOPrimaryFetch = payor;
            }
        }

        private static void soSearchMethod(int salesOrderNunmber)
        {
            soService.DataFetchServiceResponseUsingSalesOrder salesOrderFetch = PublicVars.currentClientObject.soAction.SalesOrderFetchByBrightreeID(salesOrderNunmber.ToString());
            SOFetch = salesOrderFetch.Items[0];            
        }

        private static void ptSearchMethod(int patientKey)
        {
            ptService.DataFetchServiceResponseUsingPatient patientFetch = PublicVars.currentClientObject.ptAction.PatientFetchByBrightreeID(patientKey.ToString());
            PTFetch = patientFetch.Items[0];
        }

        private static void ptPrimaryPayorSearchMethod(int patientKey)
        {
            ptService.DataFetchServiceResponseUsingPatientPayor ptPayorFetch = PublicVars.currentClientObject.ptAction.PatientPayorFetchAll(patientKey);

            foreach(ptService.PatientPayor patientPayorResponse in ptPayorFetch.Items)
            {
                if(patientPayorResponse.payorLevel == ptService.PayorLevel.Primary)
                {
                    PTPrimaryPayor = patientPayorResponse;
                    break;
                }
            }

        }
    }
}
