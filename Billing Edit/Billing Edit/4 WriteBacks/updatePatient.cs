﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    public class updatePatient
    {
        public static void updatePatientAddress(eligiblePatientReponse patient, ptService.Patient patientFetch, ptService.PatientPayor primaryPayorFetch)
        {
            //Update Insured Address Info
            primaryPayorFetch.Insured.Address.AddressLine1 = patient.address.Street1;
            primaryPayorFetch.Insured.Address.AddressLine2 = patient.address.Street2;
            primaryPayorFetch.Insured.Address.City = patient.address.City;
            primaryPayorFetch.Insured.Address.State = patient.address.State;

            //Update Billing Address Info
            patientFetch.PatientGeneralInfo.BillingAddress.AddressLine1 = patient.address.Street1;
            patientFetch.PatientGeneralInfo.BillingAddress.AddressLine2 = patient.address.Street2;
            patientFetch.PatientGeneralInfo.BillingAddress.City = patient.address.City;
            patientFetch.PatientGeneralInfo.BillingAddress.State = patient.address.State;

            //Push Updates to Brightree
            ptService.DataWriteServiceResponse billingResponse = PublicVars.currentClientObject.ptAction.PatientUpdate(patientFetch.BrightreeID, patientFetch);
            ptService.DataWriteServiceResponse insuredResponse = PublicVars.currentClientObject.ptAction.PatientPayorUpdate(primaryPayorFetch.BrightreeID, primaryPayorFetch);
            if (billingResponse.Success != true || insuredResponse.Success != true)
                Console.WriteLine("Patient Address Update Failed");
        }
    }
}
