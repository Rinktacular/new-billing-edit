﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using eligibilityMethodLibrary;

namespace Billing_Edit
{
    public class MedicareChecks
    {
        private static List<string> procCodesToCheck = new List<string>();

        public static void checkMedicareConditions(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkMedicareConditions"))
                return;

            //obivously... if the payor is not PA Medicare then we should not do the Medicare checks.
            if(salesOrderPatientFetches.PTPrimaryPayor.PayorKey == 103)
            {
                medicarePapNarrative(selectedOrder);
                checkItemCodes(selectedOrder);
                addMonthSupplyNarrative(selectedOrder);
                eligibilityCheck();
            }           
        }

        private static void medicarePapNarrative(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("medicarePAPNarrative"))
                return;
            bool needsNarrative = false;
            foreach(soService.SalesOrderItemInfo soItemInfo in selectedOrder.SalesOrderItems)
            {
                if(soItemInfo.ExtAllowAmt > 0 || soItemInfo.ProcCode == "A7030" || soItemInfo.ProcCode == "A7032" || soItemInfo.ProcCode == "A7033" || soItemInfo.ProcCode == "A7034" ||
                    soItemInfo.ProcCode == "A7031" || soItemInfo.ProcCode == "A7044" || soItemInfo.ProcCode == "A7027" || soItemInfo.ProcCode == "A7028" || soItemInfo.ProcCode == "A7029" ||
                    soItemInfo.ProcCode == "A7046" || soItemInfo.ProcCode == "A7037" || soItemInfo.ProcCode == "A4604" || soItemInfo.ProcCode == "A7038" || soItemInfo.ProcCode == "A7039" ||
                    soItemInfo.ProcCode == "A7035" || soItemInfo.ProcCode == "A7036" || soItemInfo.ProcCode == "E0561" || soItemInfo.ProcCode == "E0562")
                {
                    if(soItemInfo.ClaimNote == null)
                    {
                        needsNarrative = true;
                        break;
                    }
                    else if(!soItemInfo.ClaimNote.Contains("E0601") && !soItemInfo.ClaimNote.Contains("E0470") && !soItemInfo.ClaimNote.Contains("E0471"))
                    {
                        needsNarrative = true;
                        break;
                    }                    
                }
            }
            
            foreach(soService.SalesOrderItemInfo soItemInfo in selectedOrder.SalesOrderItems)
            {
                if(soItemInfo.ProcCode == "E0601" || soItemInfo.ProcCode == "E0470" || soItemInfo.ProcCode == "E0471")
                {
                    needsNarrative = false;
                    break;
                }
            }

            if(needsNarrative == true)
            {
                string narrative = getNarrative(selectedOrder.SalesOrderClinicalInfo.Patient.BrightreeID);
                if(narrative != null)
                {
                    foreach(soService.SalesOrderItemInfo soItemInfo in selectedOrder.SalesOrderItems)
                    {
                        if (soItemInfo.ExtAllowAmt > 0 && (soItemInfo.ProcCode == "A7030" || soItemInfo.ProcCode == "A7032" || soItemInfo.ProcCode == "A7033" || soItemInfo.ProcCode == "A7034" ||
                           soItemInfo.ProcCode == "A7031" || soItemInfo.ProcCode == "A7044" || soItemInfo.ProcCode == "A7027" || soItemInfo.ProcCode == "A7028" || soItemInfo.ProcCode == "A7029"
                           || soItemInfo.ProcCode == "A7046" || soItemInfo.ProcCode == "A7037" || soItemInfo.ProcCode == "A4604" || soItemInfo.ProcCode == "A7038" || soItemInfo.ProcCode == "A7039"
                           || soItemInfo.ProcCode == "A7035" || soItemInfo.ProcCode == "A7036" || soItemInfo.ProcCode == "E0561" || soItemInfo.ProcCode == "E0562"))
                        {
                            soItemInfo.ClaimNoteTypeKey = 1;
                            soItemInfo.ClaimNote = soItemInfo.ClaimNote + " " + narrative;
                        }
                        soService.DataWriteServiceResponse updateNarrative = PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItemInfo.BrightreeDetailID, soItemInfo);
                        if (updateNarrative.Success == false)
                        {
                            PublicVars.kickbackReasons.Add("Missing Narrative");
                            PublicVars.isEligible = true;
                            return;
                        }
                    }
                }
                else
                {
                    PublicVars.kickbackReasons.Add("Missing Narrative");
                    PublicVars.isEligible = false;
                    return;
                }
            }
        }

        private static string getNarrative(int patientKey)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("getNarrative"))
                return "";
            SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["EligibilityResults"].ConnectionString);
            dbConnection.Open();
            using (SqlCommand cmd = new SqlCommand("SELECT [ProcCode], D.ServiceDt FROM [BrightreeData].[dbo].[AllSalesOrderDetails] AS D INNER JOIN BrightreeData.dbo.[AllSalesOrders] AS S ON D.NickName = S.NickName and D.SOKey = S.SOKey WHERE S.NickName = 'StLukesHS' AND S.PtKey = '" + patientKey + "' ORDER BY D.ServiceDt ASC", dbConnection))
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        try
                        {
                            //Sometimes his request comes back null and crashes the orders.
                            if (reader.GetString(0) == "E0470" || reader.GetString(0) == "E0601" || reader.GetString(0) == "E0471")
                            {
                                string procCode = reader.GetString(0);
                                //string date = reader.GetDateTime(1);

                                //Sometimes DOS comes back as null and crashes the program...
                                DateTime DOS = reader.GetDateTime(1);
                                int monthAge = ((DateTime.Now.Year - DOS.Year) * 12) + DateTime.Now.Month - DOS.Month;

                                if (monthAge > 12)                                
                                    return "PO " + procCode + "INIT EQUIP PROVIDED " + DOS.ToString("MM") + DOS.ToString("dd") + DOS.ToString("yyyy");                                
                                else                                
                                    return "RENTING " + procCode + " INIT EQUIP PROVIDED " + DOS.ToString("MM") + DOS.ToString("dd") + DOS.ToString("yyyy");
                                
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return null;
                        }
                    }
                }
            }
            return null;
        }

        private static void checkItemCodes(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkItemCodes"))
                return;

            if (selectedOrder.DeliveryInfo.ActualDeliveryDateTime == selectedOrder.DeliveryInfo.ScheduledDeliveryDateTime)
            {
                procCodesToCheck.Add("E0143");
                procCodesToCheck.Add("E0110");
                procCodesToCheck.Add("E0163");
                procCodesToCheck.Add("E0147");
                procCodesToCheck.Add("E0168");
                procCodesToCheck.Add("E0156");
                procCodesToCheck.Add("E0114");
                procCodesToCheck.Add("E0100");
                procCodesToCheck.Add("E0149");

                foreach (soService.SalesOrderItemInfo item in selectedOrder.SalesOrderItems)
                {
                    if (procCodesToCheck.Contains(item.ProcCode))
                    {
                        selectedOrder.DeliveryInfo.ActualDeliveryDateTime = selectedOrder.DeliveryInfo.ActualDeliveryDateTime.AddDays(2);
                        PublicVars.currentClientObject.soAction.SalesOrderUpdate((int)selectedOrder.BrightreeID, selectedOrder);
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("Added Two days to Actual Date");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                }
            }

        }

        private static void addMonthSupplyNarrative(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("addMonthSupplyNarrative"))
                return;
            foreach (soService.SalesOrderItemInfo soItem in selectedOrder.SalesOrderItems)
            {
                if (soItem.ClaimNote == null || !soItem.ClaimNote.Contains("SUPPLY") || !soItem.ClaimNote.Contains("supply") || !soItem.ClaimNote.Contains("Supply"))
                    if (soItem.ProcCode == "A7038")
                        if (soItem.BillQty == 6)
                        {
                            soItem.ClaimNoteTypeKey = 1;
                            soItem.ClaimNote = soItem.ClaimNote + " 90 DAY SUPPLY";
                            PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                        }
                        else if (soItem.BillQty == 4)
                        {
                            soItem.ClaimNoteTypeKey = 1;
                            soItem.ClaimNote = soItem.ClaimNote + " 60 DAY SUPPLY";
                            PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                        }

                        else if (soItem.ProcCode == "A7031")
                            if (soItem.BillQty == 2)
                            {
                                soItem.ClaimNoteTypeKey = 1;
                                soItem.ClaimNote = soItem.ClaimNote + " 60 DAY SUPPLY";
                                PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                            }
                            else if (soItem.BillQty == 3)
                            {
                                soItem.ClaimNoteTypeKey = 1;
                                soItem.ClaimNote = soItem.ClaimNote + " 90 DAY SUPPLY";
                                PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                            }
                            else if (soItem.ProcCode == "A0732" || soItem.ProcCode == "A7033")
                                if (soItem.BillQty == 4)
                                {
                                    soItem.ClaimNoteTypeKey = 1;
                                    soItem.ClaimNote = soItem.ClaimNote + " 60 DAY SUPPLY";
                                    PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                                }
                                else if (soItem.BillQty == 5 || soItem.BillQty == 6)
                                {
                                    soItem.ClaimNoteTypeKey = 1;
                                    soItem.ClaimNote = soItem.ClaimNote + " 90 DAY SUPPLY";
                                    PublicVars.currentClientObject.soAction.SalesOrderUpdateItem((int)selectedOrder.BrightreeID, soItem.BrightreeDetailID, soItem);
                                }

            }

        }

        private static void eligibilityCheck()
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("eligibilityCheck"))
                return;

            string DOS = salesOrderPatientFetches.SOFetch.DeliveryInfo.ActualDeliveryDateTime.ToShortDateString();
            string lastName = salesOrderPatientFetches.PTFetch.PatientGeneralInfo.Name.Last;
            string firstName = salesOrderPatientFetches.PTFetch.PatientGeneralInfo.Name.First;
            string policyNumber = salesOrderPatientFetches.PTPrimaryPayor.Policy.PolicyNumber;
            string DOB = salesOrderPatientFetches.PTFetch.PatientGeneralInfo.BirthDate.ToShortDateString();
            bool needsKickback;

            patientEligibility patientaddress = new patientEligibility();
            patientaddress = eligibilityMethodLibrary.Eligibility.medicareInsuredVerification(patientaddress, firstName, lastName, policyNumber, DOB, DOS);
            eligiblePatientReponse patient = createEligibleResponse(patientaddress, lastName, firstName, policyNumber, DOB);

            if (patient.address.postalCode != "")
            {
                needsKickback = SQLMethods.zipCodeSearch(salesOrderPatientFetches.PTPrimaryPayor.Insured.Address.PostalCode, patient.address.postalCode);
                if(needsKickback == false)
                {
                    updatePatient.updatePatientAddress(patient, salesOrderPatientFetches.PTFetch, salesOrderPatientFetches.PTPrimaryPayor);
                }
            }
        }

        public static eligiblePatientReponse createEligibleResponse(patientEligibility patientAddress, string lastName, string firstName, string policyNumber, string DOB)
        {
            
            eligiblePatientReponse newPatient = new eligiblePatientReponse();

            if (!PublicVars.currentSalesOrderObject._permissions.Contains("createEligibleResponse"))
                return newPatient;

            newPatient.address.Street1 = patientAddress.address1;
            newPatient.address.Street2 = patientAddress.address2;
            newPatient.address.postalCode = patientAddress.zip;
            newPatient.address.City = patientAddress.city;

            newPatient.DOB = Convert.ToDateTime(DOB);
            newPatient.policyNumber = policyNumber;
            newPatient.lastName = lastName;
            newPatient.firstName = firstName;          

            return newPatient;
        }
    }
}
