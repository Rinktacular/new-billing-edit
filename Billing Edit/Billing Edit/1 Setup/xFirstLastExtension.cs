﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    static class xFirstLastExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        //public static string GetFirst(this string firstsource, int first_length)
        //{
        //    if (first_length <= firstsource.Length)
        //        return firstsource;
        //    return firstsource.Substring(firstsource.Length - first_length);
        //}


        public static string GetFirst(this string firstsource, int first_length)
        {
            return firstsource.Substring(0, Math.Min(firstsource.Length, first_length));
        }



    }
}
