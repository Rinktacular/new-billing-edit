﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SQL = System.Data;

namespace Billing_Edit
{
    public class SQLMethods
    {
        public struct ZipResponse
        {
            public string state { get; set; }
            public string zip { get; set; }
            public string locationtype { get; set; }
            public string cbaArea { get; set; }
        }

        public static void logErrors(string SOKey, string kickbackReason, decimal chargeAmount, string pass)
        {
            string connectionstring = "Data Source=104.209.184.65;Initial Catalog=BrightreeData;Persist Security Info=True; Connection Timeout = 60;User ID=rhumphries;Password=Qmes2015";
            SqlConnection cnn = new SqlConnection(connectionstring); 
            cnn.Open();            
            SqlCommand updateTable = new SqlCommand("INSERT INTO [Tyler].[dbo].[BillingEdit_KickbackLog] (Tyler.dbo.BillingEdit_kickbackLog.SOKey, Tyler.dbo.BillingEdit_kickbackLog.KickbackReason, Tyler.dbo.BillingEdit_kickbackLog.KickbackDate, Tyler.dbo.BillingEdit_kickbackLog.ChargedAmount, Tyler.dbo.BillingEdit_KickbackLog.Confirmed) VALUES ('"+SOKey+"','"+kickbackReason+"','"+DateTime.Now+"','"+chargeAmount.ToString()+"','"+pass+"')");
            updateTable.Connection = cnn;
            updateTable.ExecuteNonQuery();
            PublicVars.allowedAmnt = 0;            
            cnn.Close();
        }
        
        public static bool zipCodeSearch(string brightreeZip, string eligibleAPIZip)
        {
            ZipResponse brightreeInfo = new ZipResponse();
            ZipResponse eligibleAPIInfo = new ZipResponse();

            SqlConnection cnn = new SqlConnection("Data Source=104.209.184.65;Connection Timeout=60;Initial Catalog=BrightreeData;Persist Security Info=True;User ID=rhumphries;Password=Qmes2015");
            string sqlCommand = "SELECT [STATE], [ZIP2], [ZIP LOCATION TYPE], [CBA AREA] FROM [Tyler].[dbo].[MedicareZips] WHERE [ZIP2] IN ('" + brightreeZip.Substring(0, 5) + "','" + eligibleAPIZip.Substring(0, 5) + "')";

            cnn.Open();
            SQL.DataSet ds = new SQL.DataSet();
            SQL.DataTable dtable = new SQL.DataTable();
            SqlDataAdapter dscmd = new SqlDataAdapter(sqlCommand, cnn);

            dscmd.Fill(dtable);

            for(int i = 0; i < dtable.Rows.Count; i++)
            {
                if(dtable.Rows[i]["ZIP2"].ToString() == brightreeZip.Substring(0, 5))
                {
                    brightreeInfo.zip = dtable.Rows[i]["ZIP2"].ToString();
                    brightreeInfo.state = dtable.Rows[i]["STATE"].ToString();
                    brightreeInfo.locationtype = dtable.Rows[i]["ZIP LOCATION TYPE"].ToString();
                    if (dtable.Rows[i]["CBA AREA"].ToString() != null)
                        brightreeInfo.cbaArea = dtable.Rows[i]["CBA AREA"].ToString();
                    else
                        brightreeInfo.cbaArea = "";
                }
                if(dtable.Rows[i]["ZIP2"].ToString() == eligibleAPIZip.Substring(0, 5))
                {
                    eligibleAPIInfo.zip = dtable.Rows[i]["ZIP2"].ToString();
                    eligibleAPIInfo.state = dtable.Rows[i]["STATE"].ToString();
                    eligibleAPIInfo.locationtype = dtable.Rows[i]["ZIP LOCATION TYPE"].ToString();
                    if (dtable.Rows[i]["CBA AREA"].ToString() != null)
                        eligibleAPIInfo.cbaArea = dtable.Rows[i]["CBA AREA"].ToString();
                    else
                        eligibleAPIInfo.cbaArea = "";
                }
            }
            cnn.Close();

            if(eligibleAPIInfo.state != brightreeInfo.state)
            {
                PublicVars.kickbackReasons.Add("Patient's State has changed");
                PublicVars.isEligible = false;
                return false;
            }
            else if(eligibleAPIInfo.locationtype != brightreeInfo.locationtype)
            {
                PublicVars.kickbackReasons.Add("Patient's Location Type has changed");
                PublicVars.isEligible = false;
                return false;
            }
            else if(eligibleAPIInfo.cbaArea != brightreeInfo.cbaArea)
            {
                PublicVars.kickbackReasons.Add("Patient's CBA Area has changed");
                PublicVars.isEligible = false;
                return false;
            }
            return true;
        }

        public static string checkPayorCrosswalk(int payorKey)
        {
            SqlConnection cnn = new SqlConnection("Data Source=104.209.184.65;Connection Timeout=60;Initial Catalog=BrightreeData;Persist Security Info=True;User ID=rhumphries;Password=Qmes2015");
            string sqlCommand = "SELECT [Entity Payor ID] FROM [Tyler].[dbo].[payercrosswalk] WHERE [BTID] = '" + payorKey + "'";

            cnn.Open();
            SQL.DataSet ds = new SQL.DataSet();
            SQL.DataTable dtable = new SQL.DataTable();
            SqlDataAdapter dscmd = new SqlDataAdapter(sqlCommand, cnn);

            dscmd.Fill(dtable);
            cnn.Close();
            return dtable.Rows[0]["Entity Payor ID"].ToString();
        }

        public static List<string> getPermissions(string database)
        {
            List<string> permissions = new List<string>();
            SqlConnection cnn = new SqlConnection("Data Source=104.209.184.65;Connection Timeout=60;Initial Catalog=BrightreeData;Persist Security Info=True;User ID=rhumphries;Password=Qmes2015");
            string sqlCommand = "SELECT * FROM [Tyler].[dbo].[BillingEdit_PermissionList] WHERE [DatabaseName] = '" + database + "'";

            cnn.Open();
            SQL.DataSet ds = new SQL.DataSet();
            SQL.DataTable dtable = new SQL.DataTable();
            SqlDataAdapter dscmd = new SqlDataAdapter(sqlCommand, cnn);

            dscmd.Fill(dtable);

            permissions.Add(dtable.Rows[0]["DatabaseName"].ToString());
            foreach(SQL.DataColumn columnName in dtable.Columns)
            {
                permissions.Add(columnName.ColumnName);                         
            }            
            permissions.Remove("DatabaseName");
            cnn.Close();

            return permissions;
        }
        
    }
}
