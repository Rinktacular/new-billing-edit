﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Edit
{
    public class salesOrderSearchObject
    {
        public publicVarsObject _publicVars { get; set; }
        public List<string> _permissions { get; set; }
        public int _salesOrder { get; set; }
        public ServiceInitializers.serviceClients _clients { get; set; }

        public salesOrderSearchObject(int salesOrder, publicVarsObject publicVars, List<string> permissions, ServiceInitializers.serviceClients clients)
        {            
            this._salesOrder = salesOrder;
            this._publicVars = publicVars;
            this._permissions = permissions;
            this._clients = clients;
        }
    }
}
