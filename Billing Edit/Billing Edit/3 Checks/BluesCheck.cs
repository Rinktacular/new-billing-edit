﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using RestSharp;
using System.IO;
using OopFactory.X12.Hipaa.Eligibility;
using OopFactory.X12.Hipaa.Eligibility.Services;


namespace Billing_Edit
{
    class BluesCheck
    {
        public static string payorGroup = "";
        public static string billPayor = "";
        public static string planType = "";
        public static string coPay = "";

        public static void bluesCheckMethod()
        {
            getPayorGroup(salesOrderPatientFetches.PTPrimaryPayor);
            //If we do not find a payor, there is no point to continue this check.
            if (payorGroup == "") { return; }
            getBillPayor(salesOrderPatientFetches.PTPrimaryPayor.Insured.Address.PostalCode);
            if(billPayor == "") { return; }

            checkAlphaPrefix(salesOrderPatientFetches.PTPrimaryPayor.Policy.PolicyNumber);
            checkZirMed(salesOrderPatientFetches.PTPrimaryPayor.Insured.Name.First, salesOrderPatientFetches.PTPrimaryPayor.Insured.Name.Last, salesOrderPatientFetches.PTPrimaryPayor.Policy.PolicyNumber, salesOrderPatientFetches.PTPrimaryPayor.Insured.BirthDate.ToShortDateString(), DateTime.Today.ToShortDateString());
            checkZirmedResults();
        }

        private static void checkAlphaPrefix(string policyNumber)
        {            
            //If the first character in the policy number is not a Letter..
            if(Regex.IsMatch(policyNumber.Substring(0, 1), @"A-Za-z"))
            {
                PublicVars.kickbackReasons.Add("Missing Alpha Prefix");
                PublicVars.isEligible = false;
                return;
            }
            
            //Demensions is correct
            if (xFirstLastExtension.GetFirst(salesOrderPatientFetches.PTPrimaryPayor.Policy.PolicyNumber, 3) == "OPB")
            {
                PublicVars.isEligible = false;
                PublicVars.kickbackReasons.Add("DMENSIONS patient - billed incorrectly");
                return;
            }

            //Horizon to be reworked with Neil.

            //Anthem is incorrect, to be reworked in the Eligible API section with Neil/Todd.
        }

        private static void getPayorGroup(ptService.PatientPayor patientPayor)
        {
            string connectionstring = "Data Source=104.209.184.65;Initial Catalog=BrightreeData;Persist Security Info=True; Connection Timeout = 60;User ID=rhumphries;Password=Qmes2015";
            SqlConnection cnn = new SqlConnection(connectionstring);
            cnn.Open();
            //Is this a payor we want to evaluate?  BCBS payors only
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 Groups FROM [Tyler].[dbo].[BillingEdit_PayorGroups] WHERE BTDatabase ='" + PublicVars.currentPublicVarsObject.database + "' AND PayorKey='" + patientPayor.PayorKey.ToString() + "'");
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    payorGroup = reader[0].ToString();
                    cnn.Close();
                }
                else
                {
                    //Console.WriteLine("Payor does not exist in the {0} database", "Tyler.dbo.BillingEdit_PayorGroups");
                    cnn.Close();
                    return;
                }
            }                            
        }

        private static void getBillPayor(string zipCode)
        {
            if(zipCode == null || zipCode == "")
            {
                PublicVars.kickbackReasons.Add("Insured Zipcode is missing");
                PublicVars.isEligible = false;
                return;
            }
            else
                zipCode = zipCode.Substring(0, 5);

            string connectionstring = "Data Source=104.209.184.65;Initial Catalog=BrightreeData;Persist Security Info=True; Connection Timeout = 60;User ID=rhumphries;Password=Qmes2015";
            SqlConnection cnn = new SqlConnection(connectionstring);
            cnn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 BillRegion FROM [Tyler].[dbo].[BillingEdit_ZipRegions] WHERE BillingZip='" + zipCode + "'");
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                billPayor = reader[0].ToString();
                cnn.Close();
            }
            else
            {
                PublicVars.kickbackReasons.Add("Zip Code out of area");
                PublicVars.isEligible = false;
                cnn.Close();
                return;
            }
        }

        private static void checkZirMed(string ptFirstNameData, string ptLastNameData, string ptPolicyNum, string ptDOBData, string DOSData)
        {
            string zirmedUserID = "TylerZucchiatti3";
            string zirmedPassword = "ricH0300";
            string custID = "30973";
            string verCompany = "Royal Medical Supply";
            string insCoName = "Capital";
            string ptPolicyID = ptPolicyNum;
            string ptFirstName = ptFirstNameData;
            string ptLastName = ptLastNameData;
            string ptDOB = ptDOBData;
            string ptRel = "S";
            string DOS = DOSData;
            string benefitType = "18";
            string zirmedPayorID = "23045";
            string taxID = "233091595";
            string providerID = "";
            string NPI = "1477500924";

            RestClient client = new RestClient(@"https://eligibilityapi.zirmed.com/1.0/Rest/Gateway/GatewayAsync.ashx?" + "UserID=" + zirmedUserID + "&Password=" + zirmedPassword + "&CustID=" + custID + "&DataFormat=SF1" +
                   "&Data=" + verCompany + "|" + insCoName + "|" + ptPolicyID + "|" + ptFirstName + "|" + ptLastName + "|" + ptDOB + "|" + ptRel + "||" + DOS + "|" + benefitType + "||" + zirmedPayorID + "||||||" + taxID + "|" + providerID + "|HPI-" + NPI + "&ResponseType=271");

            //RestClient client = new RestClient("https://eligibilityapi.zirmed.com/1.0/Rest/Gateway/GatewayAsync.ashx?UserID=TylerZucchiatti3&Password=ricH0300&CustID=30973&DataFormat=SF1&Data=Royal Medical Supply|Capital|CLV121834687001|Poehlman|Jeffrey|03/03/1954|S||08/26/2015|18||23045||||||233091595||HPI-1477500924&ResponseType=271");

            var request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            string response271 = response.Content;

            Stream response271Stream = eligibilityMethodLibrary.StringStream.GenerateStreamFromString(response271);

            var service = new EligibilityTransformationService();
            EligibilityBenefitDocument eligibilityBenefitDocument = service.Transform271ToBenefitResponse(response271Stream);
            eligibilityBenefitDocument.EligibilityBenefitResponses = eligibilityBenefitDocument.EligibilityBenefitResponses;


            if (eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos.Count < 1)
            {
                PublicVars.kickbackReasons.Add("Subscriber Info Invalid");
                return;
            }
            foreach (EligibilityBenefitInformation benefitInfo in eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos)
            {
                if (benefitInfo.ServiceTypes != null && benefitInfo.InfoType != null && benefitInfo.InPlanNetwork != null)
                    if (benefitInfo.ServiceTypes.Count > 0 && ((benefitInfo.ServiceTypes[0].Code.Contains("12")) || benefitInfo.ServiceTypes[0].Code.Contains("DM")) && benefitInfo.InfoType.Description.Contains("Co-Insurance") && benefitInfo.InPlanNetwork.Code == "Y" && (benefitInfo.Messages.Count == 0 || benefitInfo.Messages[0].Contains("DIABETIC") == false))
                    {
                        if (benefitInfo.Percentage == null)
                        {
                            coPay = "0";
                            break;
                        }
                        else
                        {
                            coPay = benefitInfo.Percentage.ToString();
                            break;
                        }
                    }
            }
            foreach (EligibilityBenefitInformation benefitInfo in eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos)
            {
                if (benefitInfo.RelatedEntities.Count > 0 && benefitInfo.RelatedEntities[0].Name.LastName == "HORIZON BCBSNJ")
                {
                    planType = "OUT OF AREA";
                    break;
                }
                else if (benefitInfo.RelatedEntities.Count > 0 && (benefitInfo.RelatedEntities[0].Name.LastName == "BCNEPA" || benefitInfo.RelatedEntities[0].Name.LastName.Contains("FPLIC")))
                {
                    planType = "BCNEPA";
                    break;
                }
                else if (benefitInfo.PlanCoverageDescription != null && (benefitInfo.PlanCoverageDescription.Contains("PERSONAL CHOICE") || benefitInfo.PlanCoverageDescription.Contains("INDEPENDENCE BLUE") || benefitInfo.PlanCoverageDescription.Contains("KEYSTONE")))
                {
                    planType = "IBC";
                    break;
                }

                else if (benefitInfo.RelatedEntities.Count > 0 && benefitInfo.RelatedEntities[0].Name.LastName.Contains("CAPITAL"))
                {
                    planType = "Highmark";
                    break;
                }
            }
            if (planType == null)
            {
                planType = "OUT OF AREA";
            }
        }

        private static void checkZirmedResults()
        {
            if (planType != "OUT OF AREA")
            {
                //evaluate IBC
                if (billPayor == "IBC" & ((planType != "IBC" & payorGroup != "Highmark") | (planType == "IBC" & payorGroup != "IBC")))
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Wrong blue payor selected");
                    return;
                }

                //evaluate NEPA
                else if (billPayor == "NEPA" & ((planType != "BCNEPA" & payorGroup != "Highmark") | (planType == "BCNEPA" & payorGroup != "NEPA")))
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Wrong blue payor selected");
                    return;
                }

                //evaluate NJ
                else if (billPayor == "New Jersey" & payorGroup != "New Jersey")
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Wrong blue payor selected");
                    return;
                }

                //evaluate Delaware
                else if (billPayor == "Delaware" & payorGroup != "Delaware")
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Wrong blue payor selected");
                    return;
                }

                //evaluate Central/capital
                else if (billPayor == "Central" & (planType != "CAPITAL BLUE CROSS" & payorGroup != "Highmark") | (planType == "CAPITAL BLUE CROSS" & payorGroup != "Capital"))
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Wrong blue payor selected");
                    return;
                }

            }
        }
    }
}

