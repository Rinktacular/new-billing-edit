﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Billing_Edit
{
    public class salesOrderChecks
    {
        public static void salesOrderCheck(soService.SalesOrder selectedOrder, ptService.PatientPayor patientPayor)
        {
            checkPlaceOfService(selectedOrder);
            checkCoverageVerified(selectedOrder);
            checkWIPAssignedTo(selectedOrder);
            checkDoctorNPI(selectedOrder);
            checkPolicyNumber(selectedOrder);
            checkActualDate(selectedOrder);
            checkReferringProvider(selectedOrder);
            checkDiagnosisCodes(selectedOrder);
            checkMarketingReferral(selectedOrder);
            checkNYMedicaidPolicy(patientPayor);
        }

        private static void checkPlaceOfService(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkPlaceOfService"))
                return;

            if (selectedOrder.SalesOrderGeneralInfo.PlaceOfService == null || selectedOrder.SalesOrderGeneralInfo.PlaceOfService.ID != 4)
            {
                selectedOrder.SalesOrderGeneralInfo.PlaceOfService = new soService.LookupValue();
                selectedOrder.SalesOrderGeneralInfo.PlaceOfService.ID = 4;
                PublicVars.currentClientObject.soAction.SalesOrderUpdate((int)selectedOrder.BrightreeID, selectedOrder);
            }
        }

        private static void checkCoverageVerified(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkCoverageVerified"))
                return;

            if(selectedOrder.SalesOrderInsuranceInfo.CoverageVerified != true)
            {
                selectedOrder.SalesOrderInsuranceInfo.CoverageVerified = true;
                PublicVars.currentClientObject.soAction.SalesOrderUpdateInsurance((int)selectedOrder.BrightreeID, selectedOrder.SalesOrderInsuranceInfo);
            }

        }

        private static void checkNYMedicaidPolicy(ptService.PatientPayor patientPayor)
        {                                                              
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkNYMedicaidPolicy"))
                return;

            if(SQLMethods.checkPayorCrosswalk(patientPayor.PayorKey) == "11315")
                if(Regex.IsMatch(patientPayor.Policy.PolicyNumber, @"^[A-Za-z]{2}[0-9]{5}[A-Za-z]$") != true)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("NY Medicaid Policy Number format is incorrect");
                }
        }

        private static void checkWIPAssignedTo(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkWIPAssignedTo"))
                return;

            if (selectedOrder.SalesOrderWIPInfo.WIPAssignedToKey == null)
            {
                PublicVars.kickbackReasons.Add("WIP not assigned to confirmer");
                PublicVars.isEligible = false;
            }
        }

        private static void checkDoctorNPI(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkDoctorNPI"))
                return;
            if(selectedOrder.SalesOrderClinicalInfo.OrderingDoctor == null)
            {
                PublicVars.kickbackReasons.Add("Ordering Doctor is not present");
                PublicVars.isEligible = false;
            }
            else if(selectedOrder.SalesOrderClinicalInfo.OrderingDoctor.NPI == null 
                || selectedOrder.SalesOrderClinicalInfo.OrderingDoctor.NPI.Length < 1)
            {
                PublicVars.kickbackReasons.Add("Missing Ordering Doctor NPI");
                PublicVars.isEligible = false;
            }
        }

        private static void checkPolicyNumber(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkPolicyNumber"))
                return;
            try
            {
                if (selectedOrder.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.PolicyNumber == null || selectedOrder.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.PolicyNumber.Length < 1)
                {
                    PublicVars.kickbackReasons.Add("No Policy Number Present");
                    PublicVars.isEligible = false;
                    return;
                }
            }
            catch
            {

            }            
        }

        private static void checkActualDate(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkActualDate"))
                return;

            if(selectedOrder.DeliveryInfo.ActualDeliveryDateTime == Convert.ToDateTime("01/01/0001"))
            {
                PublicVars.kickbackReasons.Add("Missing Actual Date"); 
                PublicVars.isEligible = false;
            }
        }

        private static void checkReferringProvider(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkReferringProvider"))
                return;

            if(selectedOrder.SalesOrderClinicalInfo.RenderingProvider.Doctor != null)
            {
                PublicVars.kickbackReasons.Add("Referring Provider must be blank");
                PublicVars.isEligible = false;
            }
        }

        private static void checkMarketingReferral(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkMarketingReferral"))
                return;

            if(selectedOrder.SalesOrderGeneralInfo.SalesOrderClassification != null &&
                selectedOrder.SalesOrderGeneralInfo.SalesOrderClassification.ID != 38 &&
                selectedOrder.SalesOrderGeneralInfo.InventoryLocation != null &&
                selectedOrder.SalesOrderGeneralInfo.InventoryLocation.Value != "Centralized PAP Fulfillment")
            {
                try
                {
                    if (selectedOrder.SalesOrderClinicalInfo.MarketingReferral.ReferralType == soService.ReferralType.Patient
                       && lineItemValidations.SOTotalAllow(selectedOrder) > 0)
                    {
                        PublicVars.kickbackReasons.Add("Marketing Referral Type is patient. Double Check this is correct.");
                        PublicVars.isEligible = false;
                    }
                    else if (selectedOrder.SalesOrderClinicalInfo.MarketingReferral.Equals(null) && lineItemValidations.SOTotalAllow(selectedOrder) > 0)
                    {
                        PublicVars.kickbackReasons.Add("Marketing Referral can not be blank");
                        PublicVars.isEligible = false;
                    }
                   
                }catch
                {
                    PublicVars.kickbackReasons.Add("Marketing Referral can not be blank");
                    PublicVars.isEligible = false;
                }
                
            }
        }

        private static void checkDiagnosisCodes(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkDiagnosisCodes"))
                return;
            List<string> diagnosisCodes = new List<string>();
            foreach(soService.ICDCodeInfo diagCodes in selectedOrder.SalesOrderClinicalInfo.DiagnosisCodes)
            {
                if(diagnosisCodes.Contains(diagCodes.ICDCode))
                {
                    PublicVars.kickbackReasons.Add("Repeat Diagnosis Codes");
                    PublicVars.isEligible = false;
                    break;
                }
                diagnosisCodes.Add(diagCodes.ICDCode);
            }
        }    

        private static void checkHCAPPlan(soService.SalesOrder selectedOrder)
        {
            if (!PublicVars.currentSalesOrderObject._permissions.Contains("checkHCAPPlan"))
                return;

            if((xFirstLastExtension.GetFirst(selectedOrder.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.PolicyNumber, 3) == "YLS" || 
                xFirstLastExtension.GetFirst(selectedOrder.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.PolicyNumber, 3) == "890" &&
                selectedOrder.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.BrightreeID != 1451))
            {
                PublicVars.kickbackReasons.Add("HCAP plan billed incorrectly");
                PublicVars.isEligible = false;
            }
        }
  

    }
}
