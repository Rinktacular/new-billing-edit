﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class lineItemValidations
    {
        public static List<string> procCodesToCheck = new List<string>();

        public static void executeLineItemChecks(soService.SalesOrder selectedOrder)
        {
            LineItemValidationMethod(selectedOrder);
        }

        private static void LineItemValidationMethod(soService.SalesOrder selectedOrder)
        {
            decimal totalCost = 0;

            //This line is the reason for crashes 99% of the time with some weird error via the API response.  Retrying the search for every crash has stop it from happening completly.
            try
            {
                docService.DataFetchServiceResponseUsingPAR PAR = PublicVars.currentClientObject.docAction.PARFetchBySalesOrderBrightreeID((int)selectedOrder.BrightreeID, docService.vAPIPARPARLoadChildrenBitFlags.LoadAll);
            }
            catch
            {
                LineItemValidationMethod(selectedOrder);
            }

            foreach(soService.SalesOrderItemInfo soItemInfo in selectedOrder.SalesOrderItems)
            {
                totalCost = totalCost + soItemInfo.AllowAmt;

                //for tracking kickback Sales Order totals
                PublicVars.allowedAmnt = totalCost;

                if(totalCost >= 1000 && selectedOrder.SalesOrderInsuranceInfo.Payors.Count() > 0 && selectedOrder.SalesOrderInsuranceInfo.Payors.Count() < 2)
                {
                    PublicVars.kickbackReasons.Add("Sales Order charge Amount is over $1000");
                    PublicVars.isEligible = false;
                    return;
                }

                if(soItemInfo.DiagnosisCodes == null || soItemInfo.DiagnosisCodes[0].ICDCode == "")
                {
                    PublicVars.kickbackReasons.Add(soItemInfo.ItemName + " : missing Diagnosis Code");
                    PublicVars.isEligible = false;
                    return;
                }

                if(soItemInfo.ItemID == "Warrenty CPAP/BIPAP" || soItemInfo.ItemID=="Warrenty BIPAPST/ASV")
                {
                    if(soItemInfo.PriceOverride == soService.PriceOverride.True)
                    {
                        PublicVars.kickbackReasons.Add("Price Override can not be done");
                        PublicVars.isEligible = false;
                        return;
                    }
                }

                if(soItemInfo.ProcCode == null)
                {
                    PublicVars.kickbackReasons.Add("No HCPCS. Check Validations");
                    PublicVars.isEligible = false;
                    return;
                }

                if(soItemInfo.AllowAmt == soItemInfo.ChargeAmt && soItemInfo.ChargeAmt > 0 && selectedOrder.SalesOrderInsuranceInfo.Payors[1].IncludeOnSO == true)
                {
                    PublicVars.kickbackReasons.Add("Charge Amount can not equal Allowed Amount");
                    PublicVars.isEligible = false;
                    return;
                }

                if(soItemInfo.ItemName == "K0915" && soItemInfo.BillQty != 1)
                {
                    PublicVars.kickbackReasons.Add("K0915 can not have Billing Qty more than 1");
                    PublicVars.isEligible = false;
                    return;
                }

                if (soItemInfo.ProcCode == "XZERO" & soItemInfo.ChargeAmt > 0)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("XZERO item with charge amount");
                    return;
                }
                // if purchase w/ rental modifier
                if (soItemInfo.SaleType == soService.PriceType.Purchase & (soItemInfo.Modifier1 == "RR" | soItemInfo.Modifier2 == "RR" | soItemInfo.Modifier3 == "RR" | soItemInfo.Modifier4 == "RR"))
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Purchase item with RR modifier");
                    return;
                }        
               
                // if rental w/ purchase modifier
                if (soItemInfo.SaleType == soService.PriceType.Rental & (soItemInfo.Modifier1 == "NU" | soItemInfo.Modifier2 == "NU" | soItemInfo.Modifier3 == "NU" | soItemInfo.Modifier4 == "NU"))
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Rental Item with NU modifier");
                    return;
                }
                // if enteral food and bqty = orderqty
                if ((xFirstLastExtension.GetFirst(soItemInfo.ProcCode, 3) == "B41" | xFirstLastExtension.GetFirst(soItemInfo.ProcCode, 3) == "B42") & soItemInfo.BillQty == soItemInfo.Qty)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Enteral bill quantity cannot equal order quantity");
                    return;
                }
                //Billing HCPCS K0052
                if ((soItemInfo.ProcCode == "K0052" | soItemInfo.ProcCode == "A4649") & soItemInfo.ChargeAmt > 0)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Code is not a billable procedure code");
                    return;
                }
                //Max Allow Breach - E0570
                if (soItemInfo.ProcCode == "E0570" & soItemInfo.AllowAmt > 300)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("E0570 max allow breach");
                }
                //X9999 Billed to insurance
                if (soItemInfo.Payors[1].PayorUsage == soService.PayorUsages.Included & soItemInfo.ProcCode == "X9999" & soItemInfo.AllowAmt > 0)
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("X9999 billed to insurance");
                }
                //E0147 or E0265 w/incorrect modifiers
                if ((soItemInfo.Payors[1].PayorKey == 103 && (soItemInfo.ProcCode == "E0147" || soItemInfo.ProcCode == "E0265") && ((soItemInfo.Modifier1 != "NU" || soItemInfo.Modifier2 != "GA" || (soItemInfo.Modifier3 != null)) || soItemInfo.ClaimNote == "")))
                {
                    PublicVars.isEligible = false; 
                    PublicVars.kickbackReasons.Add("Incorrectly billed upgrade item");
                    return;
                }      
          
                //Duplicate modifiers
                List<string> modList = new List<string>();
                if (soItemInfo.Modifier1 != null) { modList.Add(soItemInfo.Modifier1); }
                if (soItemInfo.Modifier2 != null) { modList.Add(soItemInfo.Modifier2); }
                if (soItemInfo.Modifier3 != null) { modList.Add(soItemInfo.Modifier3); }
                if (soItemInfo.Modifier4 != null) { modList.Add(soItemInfo.Modifier4); }
                if (soItemInfo.AddMod1 != null) { modList.Add(soItemInfo.AddMod1); }
                if (soItemInfo.AddMod2 != null) { modList.Add(soItemInfo.AddMod2); }
                if (soItemInfo.AddMod3 != null) { modList.Add(soItemInfo.AddMod3); }
                if (soItemInfo.AddMod4 != null) { modList.Add(soItemInfo.AddMod4); }
                if(modList.Distinct().Count() != modList.Count())
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Duplicate modifier used");
                }
                           }
                //Check for duplicate HCPC w/ Bqty
                List<string> bqtyProc = new List<string>();
                foreach (soService.SalesOrderItemInfo soItemInfo in selectedOrder.SalesOrderItems)
                {
                    if (soItemInfo.BillQty > 0 & soItemInfo.ProcCode != "E1399" & soItemInfo.ProcCode != "B9998" & soItemInfo.ProcCode != "X9999" & soItemInfo.AllowAmt > 0)
                    {
                        bqtyProc.Add(soItemInfo.ProcCode);
                    }
                }
                if (bqtyProc.Distinct().Count() != bqtyProc.Count())
                {
                    PublicVars.isEligible = false;
                    PublicVars.kickbackReasons.Add("Duplicate HCPCS Billed");
                    return;
                }      
            }       

        public static decimal SOTotalAllow(soService.SalesOrder selectedOrder)
        {
            decimal totalAllow = 0;
            foreach(soService.SalesOrderItemInfo soItem in selectedOrder.SalesOrderItems)
            {
                totalAllow = totalAllow + soItem.AllowAmt;
            }
            return totalAllow;
        }
    }
}

