﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class evaluateOrder
    {
        public static void evaluateSalesOrder(soService.SalesOrder selectedOrder, List<string> kickbackList, bool isEligible)
        {      
                int orderNumber = selectedOrder.BrightreeID.Value;
                string currentWIP = selectedOrder.SalesOrderWIPInfo.WIPStateName;
                if (PublicVars.isEligible == true)
                {
                    try
                    {                        
                        //Confirm the order
                        soService.DataWriteServiceResponse confirmOrderResponse = PublicVars.currentClientObject.soAction.SalesOrderConfirm(orderNumber);
                        if (confirmOrderResponse.Success == false)
                        {
                            PublicVars.kickbackReasons.Add("Validation Messages On Order");
                            PublicVars.isEligible = false;
                            selectedOrder.SalesOrderGeneralInfo.User4 = PublicVars.kickbackReasons[0];
                            selectedOrder.SalesOrderWIPInfo.WIPStateKey = PublicVars.currentPublicVarsObject.kickbackWIP;
                            PublicVars.currentClientObject.soAction.SalesOrderUpdate(orderNumber, selectedOrder);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("{0} is not eligible : {1} : {2}", orderNumber, PublicVars.kickbackReasons[0], currentWIP);
                            Console.ForegroundColor = ConsoleColor.White;
                            SQLMethods.logErrors(orderNumber.ToString(), PublicVars.kickbackReasons[0], PublicVars.allowedAmnt, PublicVars.isEligible.ToString());
                        }
                        //If the confirmation failed kick back the order
                        else if (PublicVars.kickbackReasons.Contains("Validation Messages On Order") == true)
                        {
                            selectedOrder.SalesOrderGeneralInfo.User4 = PublicVars.kickbackReasons[0];
                            selectedOrder.SalesOrderWIPInfo.WIPStateKey = PublicVars.currentPublicVarsObject.kickbackWIP;
                            PublicVars.currentClientObject.soAction.SalesOrderUpdate(orderNumber, selectedOrder);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("{0} is not eligible : {1} : {2}", orderNumber, PublicVars.kickbackReasons[0], currentWIP);
                            Console.ForegroundColor = ConsoleColor.White;
                            SQLMethods.logErrors(orderNumber.ToString(), PublicVars.kickbackReasons[0], PublicVars.allowedAmnt, PublicVars.isEligible.ToString());
                        }
                        else
                        {
                            //Update the WIPState to the Confirmed WIP (5.0 - Pass)
                            PublicVars.currentClientObject.soAction.SalesOrderUpdateWIPState((int)selectedOrder.BrightreeID, PublicVars.currentPublicVarsObject.confirmWIP);
                            PublicVars.currentClientObject.soAction.SalesOrderUpdateWIPState(orderNumber, PublicVars.currentPublicVarsObject.confirmWIP);
                            soService.SalesOrderWIPInfo confirmWIPInfo = new soService.SalesOrderWIPInfo();
                            confirmWIPInfo.WIPStateKey = PublicVars.currentPublicVarsObject.confirmWIP;
                            confirmWIPInfo.WIPAssignedToPerson = salesOrderPatientFetches.SOFetch.SalesOrderWIPInfo.WIPAssignedToPerson;
                            selectedOrder.SalesOrderWIPInfo.WIPCompleted = true;
                            confirmWIPInfo.WIPAssignedToKey = salesOrderPatientFetches.SOFetch.SalesOrderWIPInfo.WIPAssignedToKey;
                            confirmWIPInfo.WIPStateName = ".5 Autoconfirm Pass";
                            selectedOrder.SalesOrderWIPInfo = confirmWIPInfo;

                            PublicVars.currentClientObject.soAction.SalesOrderUpdate(orderNumber, selectedOrder);

                            soService.DataFetchServiceResponseUsingSalesOrder salesOrderFetch = PublicVars.currentClientObject.soAction.SalesOrderFetchByBrightreeID(orderNumber.ToString());
                            soService.SalesOrder salesOrderData = salesOrderFetch.Items[0];
                            salesOrderData.SalesOrderWIPInfo.WIPCompleted = true;
                            salesOrderData.SalesOrderWIPInfo.WIPClosedDate = DateTime.Now;
                            PublicVars.currentClientObject.soAction.SalesOrderUpdate(orderNumber, salesOrderData);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0} was confirmed : {1}", orderNumber, currentWIP);
                            Console.ForegroundColor = ConsoleColor.White;
                            SQLMethods.logErrors(orderNumber.ToString(), string.Empty, PublicVars.allowedAmnt, PublicVars.isEligible.ToString());
                        }
                        
                    }
                    //Occurs if the order is already confirmed..?  -- note from previous billing edit.  Probably wrong.  Leaving this here just in case.
                    catch (Exception e)
                    {
                        Console.WriteLine("Crash during Confirm\n{0}", e.Message);
                        Console.ReadLine();
                    }
                }
                //if iseligbile == false..
                else
                {
                    //kick back the order
                    selectedOrder.SalesOrderGeneralInfo.User4 = PublicVars.kickbackReasons[0];
                    selectedOrder.SalesOrderWIPInfo.WIPStateKey = PublicVars.currentPublicVarsObject.kickbackWIP;
                    PublicVars.currentClientObject.soAction.SalesOrderUpdate(orderNumber, selectedOrder);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is not eligible : {1} : {2}", orderNumber, PublicVars.kickbackReasons[0], currentWIP);
                    Console.ForegroundColor = ConsoleColor.White;
                    SQLMethods.logErrors(orderNumber.ToString(), PublicVars.kickbackReasons[0], PublicVars.allowedAmnt, PublicVars.isEligible.ToString());
                }
        }

        public static bool checkKickbackStatus(bool isEligible)
        {
            if (isEligible == false)
                return true;
            else
                return false;
        }        
    }
}
