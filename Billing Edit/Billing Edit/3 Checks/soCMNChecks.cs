﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_Edit
{
    public class soCMNChecks
    {
        public static void executeCMNChecks(int salesOrderNumber)
        {
            executeCMNFetchMethod(salesOrderNumber);
        }

        private static void executeCMNFetchMethod(int salesOrderNumber)
        {
            docService.DataFetchServiceResponseUsingCMN cmnFetch = PublicVars.currentClientObject.docAction.CMNFetchBySalesOrderBrightreeID(salesOrderNumber, docService.vAPICMNCMNLoadChildrenBitFlags.LoadAll);
            foreach(docService.CMN cmnResponse in cmnFetch.Items)
            {
                if (cmnResponse.CMNGeneralInfo.LoggedDate == null)
                    break;
                else if(cmnResponse.CMNGeneralInfo.CMNForm.Value == "CMS 484" && salesOrderPatientFetches.SOFetch.SalesOrderInsuranceInfo.Payors[1].payorPolicyInfo.BrightreeID == 103)
                {
                    string testValue = cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(0)[0];
                    string testValue2 = cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(1)[0];
                    if((cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(0)[0] != null && Convert.ToInt32(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(0)[0]) > 55) ||
                       (cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(1)[0] != null && Convert.ToInt32(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(1)[0]) > 88) ||
                       (cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(0)[0] == null && cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(1)[0] == null))
                   {
                       PublicVars.kickbackReasons.Add("Incorrect saturation info");
                       PublicVars.isEligible = false;
                       break;
                   }

                    if (Convert.ToDateTime(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[0].Answer.Values.ElementAt(2)[0]) > salesOrderPatientFetches.SOFetch.DeliveryInfo.ActualDeliveryDateTime)
                    {
                        PublicVars.kickbackReasons.Add("Testing date after actual date");
                        PublicVars.isEligible = false;
                        break;
                    }

                    if (Convert.ToInt32(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[4].Answer.Values.ElementAt(0)[0]) > 4 &&
                        ((cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(1)[0] == null || Convert.ToInt32(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(1)[0]) > 88)
                        || (cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(0)[0] == null || Convert.ToInt32(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(0)[0]) > 55)
                        || (cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(2)[0] == null || Convert.ToDateTime(cmnResponse.CMNSectionBInfo.CMNQuestionAnswer[5].Answer.Values.ElementAt(2)[0]) > salesOrderPatientFetches.SOFetch.DeliveryInfo.ActualDeliveryDateTime)))
                    {
                        PublicVars.kickbackReasons.Add("Testing on 4LPM missing/invalid");
                        PublicVars.isEligible = false;
                        break;
                    }
                }
            }
        }
    }
}
